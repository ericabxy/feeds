<?xml version="1.0"?>
<interface xmlns="http://zero-install.sourceforge.net/2004/injector/interface">
  <name>GZDoom</name>
  <summary>a 3D-accelerated Doom source port</summary>

  <description>
    GZDoom is a fork of the ZDoom source port created by Christoph 
    Oelckers (Graf Zahl), who still oversees its development. First 
    released in 2005, it has versions for Windows, Linux, and MacOS.

    GZDoom remains a very popular port due to both its unprecedented 
    modding capability and graphical optimizations for modern hardware. 
    In particular, the ZScript language unique to GZDoom, combined with 
    ACS and DECORATE inherited from ZDoom, enables a high level of 
    gameplay modding and special mapping features.

    Many maps and mods, plus some total conversions and stand-alone 
    games, have been exclusive to GZDoom. This includes over a dozen 
    Cacoward winners.
  </description>

  <icon href="https://zdoom.org/assets/images/favicon.png" type="image/png"/>

  <homepage>https://zdoom.org/</homepage>

  <feed-for interface="https://ericxdu.gitlab.io/feeds/GZDoom.xml"/>

  <group license="GNU General Public License v3.0">
    <command name="run" path="opt/gzdoom/gzdoom"/>

    <!--
    <implementation version="{version}" arch="Linux-x86_64">
      <manifest-digest/>
      <archive href="https://zdoom.org/files/gzdoom/bin/gzdoom_{version}_amd64.deb"/>
    </implementation>

    <implementation version="{version}" arch="Linux-i386">
      <manifest-digest/>
      <archive href="https://zdoom.org/files/gzdoom/bin/gzdoom_{version}_i386.deb"/>
    </implementation>
    -->

    <implementation version="{version}" arch="Linux-x86_64">
      <manifest-digest/>
      <archive href="https://github.com/coelckers/gzdoom/releases/download/g{version}/gzdoom_{version}_amd64.deb"/>
    </implementation>

    <implementation version="{version}" arch="Linux-i386">
      <manifest-digest/>
      <archive href="https://github.com/coelckers/gzdoom/releases/download/g{version}/gzdoom_{version}_i386.deb"/>
    </implementation>
  </group>
</interface>
